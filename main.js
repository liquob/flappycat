var game = new Phaser.Game(400, 490, Phaser.AUTO, 'gameDiv');
var mainState = {
    preload: function() {
        game.stage.backgroundColor = '#71c5cf';
        game.load.image('cat', 'assets/cat.png');
        game.load.image('pipe', 'assets/pipe.png');
    },
    create: function() {
        game.physics.startSystem(Phaser.Physics.ARCADE);
        this.cat = this.game.add.sprite(100, 245, 'cat');
        game.physics.arcade.enable(this.cat);
        this.cat.body.gravity.y = 1000;
        game.input.onDown.add(this.jump, this);
        this.pipes = game.add.group();
        this.pipes.enableBody = true;
        this.pipes.createMultiple(20, 'pipe');
        this.timer = game.time.events.loop(1500, this.addRowOfPipes, this);
        this.score = 0;
        this.labelScore = game.add.text(20, 20, "0", {
            font: "30px Arial",
            fill: "#ffffff"
        });
        this.labelInstruction = game.add.text(8, 462, "Click to jump.", {
            font: "25px Arial",
            fill: "#ffffff"
        });
    },
    update: function() {
        if (!this.cat.inWorld)
            this.restartGame();
        if (this.cat.angle < 20)
            this.cat.angle += 1;
        game.physics.arcade.overlap(this.cat, this.pipes, this.restartGame, null, this);
    },
    jump: function() {
        this.cat.body.velocity.y = -350;
        var animation = game.add.tween(this.cat);
        animation.to({
            angle: -20
        }, 100);
        animation.start();
    },
    restartGame: function() {
        game.state.start('main');
    },
    addOnePipe: function(x, y) {
        var pipe = this.pipes.getFirstDead();
        pipe.reset(x, y);
        pipe.body.velocity.x = -200;
        pipe.checkWorldBounds = true;
        pipe.outOfBoundsKill = true;
    },
    addRowOfPipes: function() {
        var hole = Math.floor(Math.random() * 5) + 1;
        // Add the 6 pipes
        for (var i = 0; i < 8; i++)
            if (i != hole && i != hole + 1)
                this.addOnePipe(400, i * 60 + 10);
        this.score += 1;
        this.labelScore.text = this.score - 1;
    },
};
game.state.add('main', mainState);
game.state.start('main');
